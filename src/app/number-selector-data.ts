export interface NumberSelectorData {
    fields: number[][];
    i: number;
    j: number;
    pos: number[];
    mark: number[];
}
