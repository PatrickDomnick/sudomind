import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlayfieldComponent } from './playfield/playfield.component';


const routes: Routes = [
  {
    path: '', component: PlayfieldComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
